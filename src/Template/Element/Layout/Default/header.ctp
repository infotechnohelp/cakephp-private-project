<?= $this->Html->charset() ?>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?= $title ?>: <?= $this->fetch('title') ?></title>

<?= $this->Html->meta('icon') ?>


<?= $this->Html->css('PrivateProject.Layout/default'); ?>


<?php ?>

<?php
use Cake\Filesystem\File;

// @todo Get from local bower.json
$appBowerComponents = [
    'jquery/jquery.min.js',
];

foreach ($appBowerComponents as $appBowerComponent) {
    $file = new File(ROOT . DS . 'webroot/js/bower/' . $appBowerComponent, false);

    if ($file->exists()) {
        echo $this->Html->script('bower/' . $appBowerComponent);
    } else {
        echo $this->Html->script('PrivateProject.bower/' . $appBowerComponent);
    }
}
?>

<script>
    var root = [], homePageUrl = '<?= \Cake\Core\Configure::read('PrivateProject')['homePageUrl']?>',
    logoutAction = '<?= \Cake\Core\Configure::read('PrivateProject')['entrance']?>';
    root.push('<?= \Cake\Routing\Router::url('/') ?>');
</script>
<?= $this->Html->script('PrivateProject.jQueryScripts/LogoutButton'); ?>
