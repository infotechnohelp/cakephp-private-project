<?php

use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;


Router::plugin(
    'PrivateProject', ['path' => '/private-project'],
    function (RouteBuilder $routes) {
        $routes->connect('/', ['controller' => 'Users', 'action' => 'index'], ['_name' => 'root']);

        $routes->fallbacks(DashedRoute::class);
    }
);
