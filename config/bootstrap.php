<?php

\Cake\Core\Configure::write('AuthApi', [
    'loginAction' => '/' . \Cake\Core\Configure::read('PrivateProject')['entrance'],
    'authError'   => false,
]);


\Cake\Core\Plugin::load('AuthApi', ['routes' => true]);