$(function () {
    $('button[logout]').click(function () {
        $.post(root[0] + "auth-api/logout", function (response) {
            if (response.data === true) {
                window.location.href = root[0] + logoutAction;
            }
        }, 'json');
    });
});