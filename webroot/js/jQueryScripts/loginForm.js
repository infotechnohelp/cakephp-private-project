$(function () {
    $('button[submit]').click(function () {
        $.post(root[0] + "auth-api", {
            username: $('input[name="username"]').val(),
            password: $('input[name="password"]').val()
        }, function (response) {
            if (response.data !== null) {
                window.location.href = root[0] + homePageUrl;
            }
        }, 'json').fail(function (xhr, status, error) {
            $('div[error]').children('span').text(xhr.responseJSON.message);
            $('div[error]').finish().slideDown(1000).delay(1000).slideUp(1000);
        });
    });
});